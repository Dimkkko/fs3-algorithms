public class ArrayStack {
  int [] values = new int [10];
  int index = 0;


  private boolean  isEmpty() {
    return index <= 0;
  }

  public void push(int element) {
    values [index] = element;
    index = index +1;

  }
  public int pop() {

    if (isEmpty()) {
      throw new RuntimeException("Sorry, but my stack is Empty!");
    }

    int last = values[index - 1];
    index = index - 1;

    return last;

  }

}
