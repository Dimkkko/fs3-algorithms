import java.util.Iterator;

public class ArrayList <E> implements Iterable<E> {
  private Object[] data;
  private int size = 0;
  private int capacity = 10;

  public ArrayList() {
    data = new Object[capacity];
  }


  public void add(E element) {
    if (size >= capacity) {
      expand();
    }
    data[size++] = element;

  }

  private void expand() {
    capacity *= 2;

    Object[] newArray = new Object[capacity];
    System.arraycopy(data, 0, newArray, 0, data.length);
    data = newArray;
  }

  public E get(int index) {
    return(E) data[index];
  }

  public void removelast() {
    if (size < capacity / 4) {
      shrink();
    }
    data[--size] = null;
  }

  public int size() {
    return size;
  }

  public int getCapacity() {
    return capacity;
  }

  public void shrink() {
    capacity /= 2;
    Object [] newArray = new Integer[capacity];
    System.arraycopy(data, 0, newArray, 0, size);
    data = newArray;
  }

  public Iterator<E> iterator() {
    return new Iterator<E>() {
      int index = 0;

      public boolean hasNext() {
        return index < size;
      }

      public E next() {
        return(E) data[index++];

      }

      public void remove() {

      }
    };
  }
}
